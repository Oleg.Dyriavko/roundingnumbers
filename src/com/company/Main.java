package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String name1 = bufferedReader.readLine();
        String name2 = bufferedReader.readLine();

        FileInputStream fileInputStream = new FileInputStream(name1);
        FileOutputStream fileOutputStream = new FileOutputStream(name2);

        byte inBuff[] = new byte[fileInputStream.available()];
        fileInputStream.read(inBuff);

        for (double d : byteArrToDoubleList(inBuff)) {
            Integer result = (int) Math.round(d);
            fileOutputStream.write((result.toString() + " ").getBytes());
        }

        fileInputStream.close();
        fileOutputStream.close();
    }

    private static List<Double> byteArrToDoubleList(byte[] inBuff) {
        List<Double> doubles = new ArrayList<>();
        StringBuilder sb = new StringBuilder();

        for (byte b : inBuff) {
            if (b != (byte) ' ') {
                sb.append((char) b);
            } else {
                doubles.add(Double.parseDouble(sb.toString()));
                sb = new StringBuilder();
            }
        }
        doubles.add(Double.parseDouble(sb.toString()));
        return doubles;
    }
}


